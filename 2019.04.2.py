winnings = 0
for number in range(145852,616942):
  previous = -1
  double = False
  size = 1
  for c in [int(x) for x in str(number)]:
    if c < previous:
      break
    elif c == previous:
      size += 1
    elif c != previous:
      if size == 2:
        double = True
      size = 1
    
    previous = c
  else:
    if size == 2 or double:
      winnings += 1

print(winnings)