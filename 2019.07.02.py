'''

                            Online Python Interpreter.
                Code, Compile, Run and Debug python program online.
Write your code in this editor and press "Run" button to execute it.

'''
import numpy as np
from itertools import permutations 
base_input = """3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0"""

perms = permutations([5, 6, 7, 8, 9], 5) 
best_output = 0 
best_conf = None

for configurations in perms:
    print('')
    print("trying configurations ", configurations)
    input = [int(x) for x in base_input.split(',')]
    output = [0]
    configurations_states = {
        5: input[::],
        6: input[::],
        7: input[::],
        8: input[::],
        9: input[::]
    }
    print("- configuration ", end = '')
    for configuration in np.tile(configurations, 5):
        input = configurations_states[configuration]
        print("-", configuration, end = ' ')
        position = 0
        output.append(configuration)
        while True:
            #print(input)
            if position >= len(input):
                break
            instruction = str(input[position]).zfill(5)
            opcode = instruction[-2:]
            firstMode = int(instruction[2])
            secondMode = int(instruction[1])
            thirdMode = int(instruction[0])
            if opcode == "01":
                # Addition
                #print(" - finding addition at position ", position)
                result = (input[position + 1] if firstMode else input[input[position+1]]) + (input[position + 2] if secondMode else input[input[position+2]])
                input[input[position + 3]] = result
                position += 4
            elif opcode == "02":
                # Multiplication
                #print(" - finding multiplication at position ", position)
                result = (input[position + 1] if firstMode else input[input[position+1]]) * (input[position + 2] if secondMode else input[input[position+2]])
                input[input[position + 3]] = result
                position += 4
            elif opcode == "03":
                #print(" - finding input at position ", position)
                input[input[position + 1]] = output.pop()
                position += 2
            elif opcode == "04":
                #print(" - finding ouput at position ", position, '-->', result)
                result = input[position + 1] if firstMode else input[input[position+1]]
                position += 2
                output = [result]
                configurations_states[configuration] = input
                if result > best_output:
                    best_output = result
                    best_conf = configurations
                break
            elif opcode == "05":
                result = (input[position + 1] if firstMode else input[input[position+1]])
                #print("finding jump if true at position ", position, '-->', result)
                if result:
                    position = (input[position + 2] if secondMode else input[input[position+2]])
                else: 
                    position += 3
            elif opcode == "06":
                result = (input[position + 1] if firstMode else input[input[position+1]])
                #print("finding jump if false at position ", position, '-->', result)
                if not result:
                    position = (input[position + 2] if secondMode else input[input[position+2]])
                else: 
                    position += 3
            elif opcode == "07":
                result = (input[position + 1] if firstMode else input[input[position+1]]) < (input[position + 2] if secondMode else input[input[position+2]])
                #print("finding less at position ", position, '-->', result)
                input[input[position + 3]] = (1 if result else 0)
                position += 4
            elif opcode == "08":
                result = (input[position + 1] if firstMode else input[input[position+1]]) == (input[position + 2] if secondMode else input[input[position+2]])
                #print("finding equal at position ", position, '-->', result)
                input[input[position + 3]] = (1 if result else 0)
                position += 4
            elif opcode == "99":
                print(" - finding termination at position ", position)
                break
            else:
                print(" - finding unknown input")
                position += 4
        else:
            continue
        if opcode == "99":
            break
print('')
print("best output", best_output)
print("best_conf", best_conf)
            