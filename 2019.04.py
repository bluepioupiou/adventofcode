winnings = 0
for number in range(145852,616943):
    previous = -1
    double = False
    for c in [int(x) for x in str(number)]:
        if c < previous:
            break
        elif c == previous:
            double = True
        previous = c
    else:
        if double:
            winnings += 1

print(winnings)
