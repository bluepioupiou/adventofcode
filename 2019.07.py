'''

                            Online Python Interpreter.
                Code, Compile, Run and Debug python program online.
Write your code in this editor and press "Run" button to execute it.

'''
from itertools import permutations 
base_input = """3,8,1001,8,10,8,105,1,0,0,21,42,59,76,85,106,187,268,349,430,99999,3,9,102,3,9,9,1001,9,2,9,1002,9,3,9,1001,9,3,9,4,9,99,3,9,102,3,9,9,101,3,9,9,1002,9,2,9,4,9,99,3,9,102,3,9,9,1001,9,4,9,1002,9,5,9,4,9,99,3,9,102,2,9,9,4,9,99,3,9,101,3,9,9,1002,9,2,9,1001,9,4,9,1002,9,2,9,4,9,99,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,101,1,9,9,4,9,3,9,102,2,9,9,4,9,3,9,101,2,9,9,4,9,99,3,9,1002,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,101,2,9,9,4,9,99,3,9,1001,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,102,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,102,2,9,9,4,9,99,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,99,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,1,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,99"""

perms = permutations([0, 1, 2, 3, 4], 5) 
best_output = 0 
best_conf = None

for configurations in perms:
    print("trying configurations ", configurations)
    input = [int(x) for x in base_input.split(',')]
    output = [0]
    for configuration in configurations:
        print("- configuration ", configuration)
        position = 0
        output.append(configuration)
        while True:
            #print(input)
            if position >= len(input):
                break
            instruction = str(input[position]).zfill(5)
            opcode = instruction[-2:]
            firstMode = int(instruction[2])
            secondMode = int(instruction[1])
            thirdMode = int(instruction[0])
            if opcode == "01":
                # Addition
                print(" - finding addition at position ", position)
                result = (input[position + 1] if firstMode else input[input[position+1]]) + (input[position + 2] if secondMode else input[input[position+2]])
                input[input[position + 3]] = result
                position += 4
            elif opcode == "02":
                # Multiplication
                print(" - finding multiplication at position ", position)
                result = (input[position + 1] if firstMode else input[input[position+1]]) * (input[position + 2] if secondMode else input[input[position+2]])
                input[input[position + 3]] = result
                position += 4
            elif opcode == "03":
                print(" - finding input at position ", position)
                input[input[position + 1]] = output.pop()
                position += 2
            elif opcode == "04":
                print(" - finding ouput at position ", position, '-->', result)
                result = input[position + 1] if firstMode else input[input[position+1]]
                position += 2
                output = [result]
                if result > best_output:
                    best_output = result
                    best_conf = configurations
            elif opcode == "99":
                print(" - finding termination at position ", position)
                break
            else:
                print(" - finding unknown input")
                position += 4
print(best_output)
print(best_conf)
            