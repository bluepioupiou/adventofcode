base_input = [1,0,0,3,1,1,2,3,1,3,4,3,1,5,0,3,2,1,10,19,1,19,6,23,2,23,13,27,1,27,5,31,2,31,10,35,1,9,35,39,1,39,9,43,2,9,43,47,1,5,47,51,2,13,51,55,1,55,9,59,2,6,59,63,1,63,5,67,1,10,67,71,1,71,10,75,2,75,13,79,2,79,13,83,1,5,83,87,1,87,6,91,2,91,13,95,1,5,95,99,1,99,2,103,1,103,6,0,99,2,14,0,0]

for noun in range(0,100):
    for verb in range(0,100):
        print("trying", noun, verb)
        input = base_input[:]
        input[1] = noun
        input[2] = verb
        for pos, value in enumerate(input[0::4]):
            pos = pos * 4
            if input[pos] == 99:
                break
            elif input[pos] == 1:
                input[input[pos+3]] = input[input[pos+1]] + input[input[pos+2]]
            elif input[pos] == 2:
                input[input[pos+3]] = input[input[pos+1]] * input[input[pos+2]]
        if input[0] == 19690720:
            print(noun, verb, 100 * noun + verb)
            break
    else:
        continue
    break