class CalculatePositionAndVelocityProgram:
    def __init__(self, str_positions):
        self.positions = []
        self.velocities = []
        for str_position in str_positions.splitlines():
            position = []
            velocity = []
            for coordinate in str_position[1:-1].split(','):
                position.append(int(coordinate.split('=')[1]))
                velocity.append(0)
            self.positions.append(position)
            self.velocities.append(velocity)

    def apply_gravity(self):
        for num, moon_position in enumerate(self.positions):
            velocity_diff = [0, 0, 0]
            for num2, other_moon_position in enumerate(self.positions):
                if num != num2:
                    for axis_num in range(0, 3):
                        if moon_position[axis_num] > other_moon_position[axis_num]:
                            velocity_diff[axis_num] -= 1
                        elif moon_position[axis_num] < other_moon_position[axis_num]:
                            velocity_diff[axis_num] += 1
            for velocity_axis in range(0, 3):
                self.velocities[num][velocity_axis] += velocity_diff[velocity_axis]

    def apply_velocity(self):
        for num_velocity, velocity in enumerate(self.velocities):
            for num_velocity_axis, velocity_axis in enumerate(velocity):
                self.positions[num_velocity][num_velocity_axis] += velocity_axis

    def get_total_energy(self):
        total = 0
        for position, velocity in zip(self.positions, self.velocities):
            potential = kinetic = 0
            for axis in position:
                potential += abs(axis)
            for axis in velocity:
                kinetic += abs(axis)
            total += potential * kinetic
        return total

    def run(self, number_of_steps):
        print('starting with', number_of_steps, 'steps')
        for step in range(0, number_of_steps):
            self.apply_gravity()
            self.apply_velocity()
            #print(self.positions)
            #print(self.velocities)
            #print('---------')


with open(__file__ + '.input') as file:
    provided_input = file.read()

program = CalculatePositionAndVelocityProgram(provided_input)
program.run(1000)
print('solution: ')
print('last position', program.positions)
print('last velocity', program.velocities)
print('total energy', program.get_total_energy())
